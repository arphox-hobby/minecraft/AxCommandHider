package arphox.axcommandhider;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ServerVersionFinderTests {
    private static void expect(String versionString, VersionType versionType) {
        // Arrange
        ServerVersionFinder sut = new ServerVersionFinder();

        // Act
        VersionType result = sut.determineServerVersion(versionString);

        // Assert
        assertEquals(versionType, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "git-Spigot-aaaaaaa-bbbbbbb (MC: 0.7)",
            "git-Spigot-aaaaaaa-bbbbbbb (MC: 1.7)",
            "git-Spigot-aaaaaaa-bbbbbbb (MC: 1.7.12)"
    })
    public void notSupported(String versionString) {
        expect(versionString, VersionType.Unsupported);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "git-Spigot-c3c767f-33d5de3 (MC: 1.8)",
            "git-Spigot-db6de12-18fbb24 (MC: 1.8.8)",
            "git-Spigot-7d15d07-c194444 (MC: 1.9)",
            "git-Spigot-aaaaaaa-bbbbbbb (MC: 1.9.4)",
    })
    public void v8(String versionString) {
        expect(versionString, VersionType.V8);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "git-Spigot-6016ac7-10c10b3 (MC: 1.10)",
            "git-Spigot-f950f8e-0a81101 (MC: 1.11)",
            "git-Spigot-596221b-9a1fc1e (MC: 1.12)",
            "git-Spigot-aaaaaaa-bbbbbbb (MC: 1.12.2)",
    })
    public void v10(String versionString) {
        expect(versionString, VersionType.V10);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "git-Spigot-fe3ab0d-162bda9 (MC: 1.13)",
            "git-Spigot-4d24e36-5193f76 (MC: 1.14)",
            "git-Spigot-800b93f-8160e29 (MC: 1.15.2)",
            "git-Paper-143 (MC: 1.15.2)",
    })
    public void v13(String versionString) {
        expect(versionString, VersionType.V13);
    }
}