package arphox.axcommandhider.commandfilters;

import arphox.axcommandhider.Permissions;
import org.bukkit.permissions.Permissible;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SeeAllPermissionCheckerTests {
    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void ifHasPermission_ReturnsTrueAndDoesNotCallNext() {
        // Arrange
        ICommandFilter nextFilter = Mockito.mock(ICommandFilter.class);
        Mockito.when(nextFilter.isAllowed(anyString(), any(Permissible.class))).thenReturn(false);
        ICommandFilter sut = new SeeAllPermissionChecker(nextFilter);
        Permissible permissible = setupPermissible(true);

        // Act
        boolean result = sut.isAllowed("whatever", permissible);

        // Assert
        assertTrue(result);
        verify(nextFilter, times(0)).isAllowed(anyString(), any(Permissible.class));
    }

    @Test
    public void ifDoesNotHavePermission_CallsNext() {
        // Arrange
        ICommandFilter nextFilter = Mockito.mock(ICommandFilter.class);
        ICommandFilter sut = new SeeAllPermissionChecker(nextFilter);
        Permissible permissible = setupPermissible(false);
        final String command = "whatever";

        // Act
        sut.isAllowed(command, permissible);

        // Assert
        verify(nextFilter, times(1)).isAllowed(command, permissible);
    }

    @ParameterizedTest
    @ValueSource(booleans = {
            true,
            false
    })
    public void ifDoesNotHavePermission_ReturnsWhatNextReturns(boolean whatNextShouldReturn) {
        // Arrange
        ICommandFilter nextFilter = Mockito.mock(ICommandFilter.class);
        Mockito.when(nextFilter.isAllowed(anyString(), any(Permissible.class))).thenReturn(whatNextShouldReturn);
        ICommandFilter sut = new SeeAllPermissionChecker(nextFilter);
        Permissible permissible = setupPermissible(false);

        // Act
        boolean result = sut.isAllowed("whatever", permissible);

        // Assert
        assertEquals(whatNextShouldReturn, result);
    }

    private Permissible setupPermissible(boolean hasPermission){
        Permissible permissible = Mockito.mock(Permissible.class);
        Mockito.when(permissible.hasPermission(Permissions.SEE_ALL)).thenReturn(hasPermission);
        return permissible;
    }
}