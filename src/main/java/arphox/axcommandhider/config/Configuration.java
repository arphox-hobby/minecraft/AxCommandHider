package arphox.axcommandhider.config;

import java.util.Collection;

public class Configuration {
    private final Collection<String> commandBlackList;
    private final boolean filterNamespacedCommands;

    public Configuration(Collection<String> commandBlackList, boolean filterNamespacedCommands) {
        this.commandBlackList = commandBlackList;
        this.filterNamespacedCommands = filterNamespacedCommands;
    }

    public Collection<String> getCommandBlackList() {
        return commandBlackList;
    }

    public boolean getFilterNamespacedCommands() {
        return filterNamespacedCommands;
    }
}