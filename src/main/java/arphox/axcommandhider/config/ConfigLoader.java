package arphox.axcommandhider.config;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Set;

public class ConfigLoader {
    public Configuration load(JavaPlugin plugin) {
        BukkitYAMLConfigFile configFileHandler = new BukkitYAMLConfigFile(plugin, "config.yml");
        ConfigurationSection config = configFileHandler.getConfig().getRoot();
        Set<String> rootKeys = config.getKeys(false);

        List<String> commandBlackList = loadCommandBlacklist(config, rootKeys);
        boolean filterNamespacedCommands = loadFilterNamespacedCommands(config, rootKeys, configFileHandler);

        return new Configuration(commandBlackList, filterNamespacedCommands);
    }

    private List<String> loadCommandBlacklist(ConfigurationSection config, Set<String> keys) {
        if (!keys.contains("command-blacklist")) {
            throw new RuntimeException("Missing 'command-blacklist' entry in configuration root!");
        }
        return config.getStringList("command-blacklist");
    }

    private boolean loadFilterNamespacedCommands(ConfigurationSection config, Set<String> keys, BukkitYAMLConfigFile configFileHandler) {
        if (!keys.contains("filter-namespaced-commands")) {
            final boolean value = false;
            config.set("filter-namespaced-commands", value);
            configFileHandler.saveConfig();
            return value;
        }

        return config.getBoolean("filter-namespaced-commands");
    }
}