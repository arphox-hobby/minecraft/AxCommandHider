package arphox.axcommandhider;

public final class Permissions {
    public static final String SEE_ALL = "axcommandhider.seeall";

    private Permissions() {
    }
}
