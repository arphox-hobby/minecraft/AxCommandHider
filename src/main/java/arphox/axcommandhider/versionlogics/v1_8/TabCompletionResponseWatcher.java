package arphox.axcommandhider.versionlogics.v1_8;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

import static arphox.axcommandhider.versionlogics.Common.isCommand;

class TabCompletionResponseWatcher extends PacketAdapter {
    private final ICommandFilter commandFilter;

    public TabCompletionResponseWatcher(JavaPlugin plugin, ICommandFilter commandFilter) {
        super(plugin, ListenerPriority.NORMAL, PacketType.Play.Server.TAB_COMPLETE);
        this.commandFilter = commandFilter;
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        if (event.getPacketType() != PacketType.Play.Server.TAB_COMPLETE)
            return;

        String[] originalCompletions = getCompletions(event.getPacket());
        String[] newCommands = getAllowedCommands(originalCompletions, event.getPlayer());
        writeNewCommandsToPacket(event, newCommands);
    }

    private String[] getCompletions(PacketContainer packet) {
        return (String[]) packet.getModifier().read(0);
    }

    private String[] getAllowedCommands(String[] originalCommands, Player player) {
        return Arrays.stream(originalCommands)
                .filter(s -> {
                    if (isCommand(s))
                        return commandFilter.isAllowed(s, player);
                    else
                        return true;
                })
                .toArray(String[]::new);
    }

    private void writeNewCommandsToPacket(PacketEvent event, String[] newCommands) {
        event.getPacket().getModifier().write(0, newCommands);
    }
}