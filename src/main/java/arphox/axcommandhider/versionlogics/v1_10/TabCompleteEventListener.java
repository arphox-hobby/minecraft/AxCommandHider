package arphox.axcommandhider.versionlogics.v1_10;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import java.util.List;

import static arphox.axcommandhider.versionlogics.Common.isCommand;

public class TabCompleteEventListener implements Listener {
    private final ICommandFilter commandFilter;

    public TabCompleteEventListener(ICommandFilter commandFilter) {
        this.commandFilter = commandFilter;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onTabCompleteEvent(TabCompleteEvent event) {
        String messageBuffer = event.getBuffer();
        if (!isCommand(messageBuffer))
            return;

        CommandSender sender = event.getSender();
        if (messageBuffer.contains(" ") && !commandFilter.isAllowed(messageBuffer, sender))
            event.setCancelled(true);

        filterCompletions(sender, event.getCompletions());
    }

    private void filterCompletions(CommandSender sender, List<String> completions) {
        if (completions.isEmpty())
            return; // GG Java, throwing exception at removeIf if the list is empty...

        completions.removeIf(x -> !commandFilter.isAllowed(x, sender));
    }
}