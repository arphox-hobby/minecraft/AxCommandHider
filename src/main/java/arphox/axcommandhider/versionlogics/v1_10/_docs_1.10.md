## Developer documentation for handling version 1.10

Starting from 1.10, the Spigot-API contains the 
[`TabCompleteEvent`](https://helpch.at/docs/1.10/org/bukkit/event/server/TabCompleteEvent.html),
which makes it possible to:
- inspect the "buffer" (the whole message the sender typed so far)
- inspect and modify the list of completions that will be sent back to the sender.

So we subscribe to this event and act accordingly.

### `TabCompleteEvent` handling logic
#### Inspecting the message buffer
First, we inspect the message buffer:
- if the buffer is not a command, we return and do nothing
- if the buffer contains a space (`' '`), we use the command filter to check if it is a hidden command.
(the reason to check for space is the same as it is in 1.8 logic)  
  If it is a hidden command, we cancel the event.
  
#### Filtering completions
Now, the message buffer is a command which does not contain a space.

We filter the completion list using the command filter, removing hidden commands.