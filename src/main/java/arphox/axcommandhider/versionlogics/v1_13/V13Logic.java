package arphox.axcommandhider.versionlogics.v1_13;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import arphox.axcommandhider.versionlogics.IVersionLogic;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class V13Logic implements IVersionLogic {
    private final JavaPlugin plugin;
    private final ICommandFilter commandFilter;

    public V13Logic(JavaPlugin plugin, ICommandFilter commandFilter) {
        this.plugin = plugin;
        this.commandFilter = commandFilter;
    }

    @Override
    public void enable() {
        registerPlayerCommandSendEventListener();
        updateAvailableCommandsForAllPlayers();
    }

    private void registerPlayerCommandSendEventListener() {
        PlayerCommandSendEventListener listener = new PlayerCommandSendEventListener(commandFilter);
        plugin.getServer().getPluginManager().registerEvents(listener, plugin);
    }

    private void updateAvailableCommandsForAllPlayers() {
        plugin.getServer().getOnlinePlayers().forEach(Player::updateCommands);
    }
}