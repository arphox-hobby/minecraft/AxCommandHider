package arphox.axcommandhider.versionlogics;

import org.bukkit.plugin.java.JavaPlugin;

public class UnsupportedVersionLogic implements IVersionLogic {
    private final JavaPlugin plugin;

    public UnsupportedVersionLogic(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void enable() {
        plugin.getLogger().severe("Sorry, this plugin is not supported for versions earlier than 1.8.");
        plugin.getServer().getPluginManager().disablePlugin(plugin);
    }
}