package arphox.axcommandhider.commandfilters;

import arphox.axcommandhider.Permissions;
import org.bukkit.permissions.Permissible;

public class SeeAllPermissionChecker implements ICommandFilter {
    private final ICommandFilter nextFilter;

    public SeeAllPermissionChecker(ICommandFilter nextFilter) {
        this.nextFilter = nextFilter;
    }

    @Override
    public boolean isAllowed(String command, Permissible permissible) {
        if (permissible.hasPermission(Permissions.SEE_ALL))
            return true;

        return nextFilter.isAllowed(command, permissible);
    }
}