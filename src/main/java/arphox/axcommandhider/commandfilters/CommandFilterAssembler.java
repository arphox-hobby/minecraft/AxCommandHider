package arphox.axcommandhider.commandfilters;

import arphox.axcommandhider.config.Configuration;

public class CommandFilterAssembler {
    public ICommandFilter Assemble(Configuration config) {
        ICommandFilter blackListChecker = new BlackListFilter(config.getCommandBlackList(), (c, p) -> true);
        ICommandFilter namespacedCommandChecker = new NamespacedCommandFilter(config.getFilterNamespacedCommands(), blackListChecker);
        ICommandFilter rootCommandExtractor = new RootCommandNameExtractor(namespacedCommandChecker);
        ICommandFilter seeAllPermissionChecker = new SeeAllPermissionChecker(rootCommandExtractor);

        return seeAllPermissionChecker;
    }
}