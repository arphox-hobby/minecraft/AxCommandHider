# Arphox's command hider (AxCommandHider)

Spigot server plugin that allows hiding command suggestions from players.  
[Link to plugin's SpigotMC page](https://www.spigotmc.org/resources/arphoxs-command-hider.76357/)

# Sneak peek
(Image is from 1.13+)

**Before ➡** ![](https://i.imgur.com/FtbeP4E.png)
**After ➡** ![](https://i.imgur.com/Ps23ow4.png)
-
# How to use
Copy the plugin jar to your `plugins` directory, and load the plugin.  
Then it will create its initial configuration (`config.yml`) to the `AxCommandHider` directory which you can modify.  

**The plugin reads the configuration when enabled.**  
This means if you change the configuration, it is enough to restart (disable & enable) the plugin, changes will take place immediately.

## Configuration

### Overview

Example configuration:  
```yml
command-blacklist:
- 'pl'
- 'plugins'
filter-namespaced-commands: false
```

**Definition (namespaced command)**: A command in its fully qualified format: `plugin:command`. Example: `bukkit:plugins`.

### Details
#### `command-blacklist`

Tells which commands to remove from command suggestion list.  
Add commands without their _first_ slash.  
So `/about` becomes `about`, and e.g. WorldEdit's `//br` becomes `/br`.  
Commands does not have to be inside apostrophes (`'`), but it is good practice to include them 
because then you don't have to worry about commands that contain special characters
like `?`, `:` or `/`.

Example:
```yml
command-blacklist:
- 'about'         # blocks '/about'
- 'bukkit:about'  # blocks '/bukkit:about'
- '/br'           # blocks '//br'
```

#### `filter-namespaced-commands`

Boolean value. If set to `true`, the plugin does not send namespaced commands.  

_Note:_ If this setting is missing from the configuration, the plugin adds it as `false` when enabled.

## Permissions
Players with `axcommandhider.seeall` permission bypass the blacklist.

## How it works
The plugin works differently for different minecraft server versions.

### 1.13 and above
In 1.13, they changed the way how the `TabCompleteEvent` works and a new event partially took its place: the [`PlayerCommandSendEvent`](https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/player/PlayerCommandSendEvent.html).  
When a player joins the server, the server sends the available commands to the player. But before sending those commands, it fires the `PlayerCommandSendEvent`.  
This plugin subscribes to this event and modifies the list of sent commands based on the configuration.

Also, when you enable the plugin, it sends the updated list of available commands to all online players (using `Player#updateCommands()`).

### 1.10 to 1.12.2
Anytime you press `<TAB>` while chatting (player/console), the [`TabCompleteEvent`](https://helpch.at/docs/1.12.2/org/bukkit/event/server/TabCompleteEvent.html) fires.  
This plugin subscribes this event and modifies the list of sent suggestions based on the configuration.

### 1.8 to 1.9.4
**Uses [`ProtocolLib`](https://github.com/dmulloy2/ProtocolLib/releases)** to filter tab complete messages since the Spigot API doesn't really provide this functionality.  
So if you are on these versions, make sure to include ProtocolLib as well. I tested it with ProtocolLib-4.5.0.

## Notes
- Under minecraft version 1.10, it requires ProtocolLib to work! Over it, it is not needed.
- The plugin also blocks blocked commands' parameter suggestion list
- **The plugin only affects the list of commands shown** and does not prevent players to actually _use_ those commands; they just don't see visual help when they type.
- The plugin cannot block proxy (e.g. BungeeCord) commands as it is sent by the proxy and not by the Spigot server.

# Tested versions
The plugin has been developed using API version of `1.15.2-R0.1` but I tested it and it works on and over `1.8`.  

### Developer notes
If you are interested, I maintain articles about the 
[`PlayerCommandSendEvent`](https://gitlab.com/Arphox.HobbyProjects/minecraft/mcdevkb/-/blob/master/articles/events/PlayerCommandSendEvent.md) 
and [**tab completion**](https://gitlab.com/Arphox.HobbyProjects/minecraft/mcdevkb/-/blob/master/articles/tab_completion.md).

# Donate
If you like my work, consider donating. Any amount helps. :-)  
[**Click here to donate**](https://www.paypal.me/arphox).

## Why choose my plugins?
- I care about my plugins' performance and stability. If you find any room for performance improvement in my code, let me know!
- If you find a bug I will fix it because I care about the stuff I make
- I am a professional developer in my daytime job and I care a lot about software quality, emphasizing clean code
- I make my plugins open source so you are free to check what the plugin is doing

_If you chose my plugin, please let me know: write a review on the plugin's SpigotMC page!_  
Thank you!